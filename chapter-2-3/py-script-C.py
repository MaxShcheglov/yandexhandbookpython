k = int(input())
n = int(input())

output = ''
for i in range(k, n + 1):
    output += f' {i}'

output = output[1:len(output)]

print(output)