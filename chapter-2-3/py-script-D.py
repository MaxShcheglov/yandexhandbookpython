k = int(input())
n = int(input())

minimal = min(k, n)
maximal = max(k, n)

output = ''
for i in range(minimal, maximal + 1):
    output += f' {i}'

output = output[1:len(output)]

print(output + '!')