price_binary = input()
money = int(input())


def bin_to_dec(binary_string):
    result = 0
    array_length = len(binary_string)
    for i in range(array_length):
        result += int(binary_string[i]) * 2 ** (array_length - i - 1)

    return result


price = bin_to_dec(price_binary)
print(money - price)