name = input()
price = input()
weight = input()
money = input()

final_price = int(price) * int(weight)
change = int(money) - final_price


def format_line(key, value):
    TARGET_LINE_LENGTH = 35
    current_line_length = len(key + value)
    return f'{key}{" " * (TARGET_LINE_LENGTH-current_line_length)}{value}'


print('================Чек================')
print(format_line('Товар:', name))
print(format_line('Цена:', f'{weight}кг * {price}руб/кг'))
print(format_line('Итого:', f'{final_price}руб'))
print(format_line('Внесено:', f'{money}руб'))
print(format_line('Сдача:', f'{change}руб'))
print('===================================')