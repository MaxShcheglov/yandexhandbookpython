name = input()
locker_number = int(input())


def get_group_number(locker_number):
    return locker_number // 100


def get_child_number(locker_number):
    return locker_number % 10


def get_bed_number(locker_number):
    return locker_number // 10 % 10


output = f"Группа №{get_group_number(locker_number)}.\n"
output += f"{get_child_number(locker_number)}. {name}.\n"
output += f"Шкафчик: {locker_number}.\n"
output += f"Кроватка: {get_bed_number(locker_number)}."

print(output)