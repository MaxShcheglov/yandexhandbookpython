number1 = int(input())
number2 = int(input())


def get_number(a, param):
    if param == 1:
        return a // 100
    if param == 2:
        return a // 10 % 10
    if param == 3:
        return a % 10


def custom_sum(a, b):
    c = a + b
    if c < 10:
        return c
    else:
        return c - 10


result = ''
for x in range(1, 4):
    a = get_number(number1, x)
    b = get_number(number2, x)
    c = custom_sum(a, b)
    result += str(c)

print(result)