current_money = int(input())
additional_money_binary = input()


def bin_to_dec(binary_string):
    result = 0
    array_length = len(binary_string)
    for i in range(array_length):
        result += int(binary_string[i]) * 2 ** (array_length - i - 1)

    return result


additional_money = bin_to_dec(additional_money_binary)
result = current_money + additional_money
print(result)