n = int(input())
m = int(input())
t = int(input())


def func(a):
    if a < 10:
        return f"0{a}"
    else:
        return f"{a}"


minutes = n * 60 + m
arriving_minutes = minutes + t

full_number_of_hours = arriving_minutes // 60
full_number_of_days = full_number_of_hours // 24
last_day_hours = full_number_of_hours - full_number_of_days * 24
minutes = arriving_minutes - full_number_of_hours * 60

print(f"{func(last_day_hours)}:{func(minutes)}")