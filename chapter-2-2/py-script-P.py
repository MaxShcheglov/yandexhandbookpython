speed1 = int(input())
speed2 = int(input())
speed3 = int(input())

speeds = list([speed1, speed2, speed3])
speeds.sort()


def get_name_by_speed(speed1, speed2, speed3, target_speed):
    if target_speed == speed1:
        return 'Петя'
    if target_speed == speed2:
        return 'Вася'
    if target_speed == speed3:
        return 'Толя'


place1 = get_name_by_speed(speed1, speed2, speed3, speeds[-1])
place2 = get_name_by_speed(speed1, speed2, speed3, speeds[-2])
place3 = get_name_by_speed(speed1, speed2, speed3, speeds[0])

print(f'          {place1}          ')
print(f'  {place2}                  ')
print(f'                  {place3}  ')
print(f'   II      I      III   ')
