number = int(input())


def is_beautiful_number(number):
    digit1 = number // 100 % 10
    digit2 = number // 10 % 10
    digit3 = number % 10

    digit_array = [digit1, digit2, digit3]
    min_digit = min(digit_array)
    max_digit = max(digit_array)
    last_digit = 0

    for digit in digit_array:
        if (digit != min_digit) and (digit != max_digit):
            last_digit = digit

    return min_digit + max_digit == last_digit * 2


if is_beautiful_number(number):
    print('YES')
else:
    print('NO')
