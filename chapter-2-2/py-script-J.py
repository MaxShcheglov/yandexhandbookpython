number = int(input())

sum1 = (number // 10 % 10) + (number % 10)
sum2 = (number // 100 % 10) + (number // 10 % 10)

cipher = f'{max(sum1, sum2)}{min(sum1, sum2)}'

print(cipher)