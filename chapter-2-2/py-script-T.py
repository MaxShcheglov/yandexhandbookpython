one = str(input())
two = str(input())
three = str(input())

shortest_length = 1000
shortest_string = 'яяяяя'

if 'зайка' in one:
    shortest_string = one
    shortest_length = len(one)
if 'зайка' in two and two < shortest_string:
    shortest_string = two
    shortest_length = len(two)
if 'зайка' in three and three < shortest_string:
    shortest_string = three
    shortest_length = len(three)

print(f'{shortest_string} {shortest_length}') 