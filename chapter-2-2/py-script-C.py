speed1 = int(input())
speed2 = int(input())
speed3 = int(input())

name1 = 'Петя'
name2 = 'Вася'
name3 = 'Толя'

maximal_speed = max(speed1, speed2, speed3)

if maximal_speed == speed1:
    print(name1)
elif maximal_speed == speed2:
    print(name2)
else:
    print(name3)