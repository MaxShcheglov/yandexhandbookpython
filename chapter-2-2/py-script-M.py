number1 = int(input())
number2 = int(input())
number3 = int(input())

numbers = [number1, number2, number3]


def get_equal_digits_at_same_positions(numbers, position):
    if position == 1:
        if numbers[0] // 10 % 10 == numbers[1] // 10 % 10 and numbers[1] // 10 % 10 == numbers[2] // 10 % 10:
            return numbers[0] // 10 % 10 
    elif position == 2:
        if numbers[0] % 10 == numbers[1] % 10 and numbers[1] % 10 == numbers[2] % 10:
            return numbers[0] % 10
    
    return 0


equal_digit = get_equal_digits_at_same_positions(numbers, 1) + get_equal_digits_at_same_positions(numbers, 2)
print(equal_digit)
