n = int(input())
m = int(input())

apples_count1 = 7  # Петя
apples_count2 = 6  # Вася
apples_count3 = 0  # Толя
apples_count4 = 0  # Гена
apples_count5 = 0  # Дима

apples_count1 -= 3
apples_count2 += 3

apples_count2 += 5
apples_count3 -= 5
apples_count2 -= 2
apples_count4 += 2

apples_count5 -= n
apples_count1 += n
apples_count2 += m
apples_count5 -= m


def get_name_by_count(count1, count2, count3, count4, count5, count):
    if (count == count1):
        return 'Петя'
    elif (count == count2):
        return 'Вася'
    elif (count == count3):
        return 'Гена'
    elif (count == count4):
        return 'Толя'
    elif (count == count5):
        return 'Дима'


max_count = max(apples_count1, apples_count2, apples_count3, apples_count4, apples_count5)
name_with_max_count = get_name_by_count(apples_count1, apples_count2, 
                                        apples_count3, apples_count4, apples_count5, max_count)

print(name_with_max_count)