#  https://otvet.mail.ru/question/231499856

a = list(map(int, input()))
a.sort()

number1 = (a[0] * 10 + a[1]) if a[0] != 0 else (a[1] * 10 + a[0])
number2 = a[-1] * 10 + a[-2]

print(f'{number1} {number2}')