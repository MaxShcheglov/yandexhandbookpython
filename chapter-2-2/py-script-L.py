side1 = int(input())
side2 = int(input())
side3 = int(input())

sides = [side1, side2, side3]


def is_triangle(sides):
    for side in sides:
        if side >= get_other_sides_sum(sides, side):
            return False

    return True


def get_other_sides_sum(sides, target_side):
    sum = 0
    is_item_filtered = False
    for side in sides:
        if not is_item_filtered and side == target_side:
            is_item_filtered = True
            continue
        else:
            sum += side
    
    return sum


if is_triangle(sides):
    print('YES')
else:
    print('NO')
