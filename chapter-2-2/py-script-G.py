number_as_string = input()
reversed_number_as_string = reversed(number_as_string)

if (list(number_as_string) == list(reversed_number_as_string)):
    print('YES')
else:
    print('NO')