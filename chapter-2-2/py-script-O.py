numbers1 = list(map(int, input()))
numbers2 = list(map(int, input()))
numbers = list(numbers1 + numbers2)
numbers.sort()

output1 = numbers[-1]
output2 = (numbers[1] + numbers[2]) % 10
output3 = numbers[0]

print(f'{output1}{output2}{output3}')