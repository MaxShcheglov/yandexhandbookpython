speed1 = int(input())
speed2 = int(input())
speed3 = int(input())


def get_middle_speed(speed1, speed2, speed3, max_speed, min_speed):
    if (speed1 != max_speed) and (speed1 != min_speed):
        return speed1
    elif (speed2 != max_speed) and (speed2 != min_speed):
        return speed2
    elif (speed3 != max_speed) and (speed3 != min_speed):
        return speed3


def get_name_by_speed(speed1, speed2, speed3, speed):
    if (speed == speed1):
        return 'Петя'
    elif (speed == speed2):
        return 'Вася'
    elif (speed == speed3):
        return 'Толя'


max_speed = max(speed1, speed2, speed3)
min_speed = min(speed1, speed2, speed3)
middle_speed = get_middle_speed(speed1, speed2, speed3, max_speed, min_speed)

print(f'1. {get_name_by_speed(speed1, speed2, speed3, max_speed)}')
print(f'2. {get_name_by_speed(speed1, speed2, speed3, middle_speed)}')
print(f'3. {get_name_by_speed(speed1, speed2, speed3, min_speed)}')
