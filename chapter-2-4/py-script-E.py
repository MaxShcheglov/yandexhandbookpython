areas_number = int(input())

w, h = areas_number, 1000
matrix = [[0 for x in range(h)] for y in range(w)]

for i in range(w):
    for j in range(h):
        word = input()
        matrix[i][j] = word

        if word == 'ВСЁ':
            break

area_with_rabbit_number = 0

for i in range(w):
    for j in range(h):
        if matrix[i][j]:
            if matrix[i][j] == 'зайка':
                area_with_rabbit_number += 1
                break

            if matrix[i][j] == 'ВСЁ':
                break

print(area_with_rabbit_number)